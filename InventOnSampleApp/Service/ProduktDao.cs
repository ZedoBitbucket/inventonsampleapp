using System;
using System.Collections.Generic;
using System.Linq;
using InventOnSampleApp.Models;

namespace InventOnSampleApp.Service
{
    public interface IProduktDao
    {
        Produkt GetProduktById(int id);
        IEnumerable<Produkt> Search(string searchPhrase);
    }

    public class ProduktDao : IProduktDao
    {
        private readonly AppDbCtx _ctx;

        public ProduktDao()
        {
            _ctx = new AppDbCtx();
        }

        public Produkt GetProduktById(int id)
        {
            var r = _ctx.Produkt.SingleOrDefault(s => s.Id == id);
            return r;
        }

        public IEnumerable<Produkt> Search(string searchPhrase)
        {
            var idList = _ctx.Produkt.Select(id => id.Id).ToList();
            var rand = new Random();
            var lista = new List<Produkt>();
            for (int i = 0; i < rand.Next(6,25); i++)
            {
                var p = GetProduktById(idList[rand.Next(1, idList.Count - 1)]);
                lista.Add(p);
            }
            return lista.Distinct();
        }
    }
}