﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using InventOnSampleApp.Models;

namespace InventOnSampleApp.Service
{
    public interface IWynikiWyszukiwaniaService
    {
        /// <summary>
        /// Powiązane wyniki wyszukiwania
        /// </summary>
        TextValue GetPww(string searchPhrase);

        /// <summary>
        /// Najpopularniejsze wyniki wyszukiwania
        /// </summary>
        TextValue GetNww(string searchPhrase);
    }

    public class WynikiWyszukiwaniaService : IWynikiWyszukiwaniaService
    {
        public TextValue GetPww(string searchPhrase)
        {
            return new TextValue()
            {
                Text = "kategoria 1/podkategoria 1/produkty",
                Value = "A"
            };
        }

        public TextValue GetNww(string searchPhrase)
        {
            //dao update searchPhrase

            return new TextValue()
            {
                Text = "kategoria 1/podkategoria 1/produkty",
                Value = "B"
            };
        }
    }
}