﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using InventOnSampleApp.Models;

namespace InventOnSampleApp.Service
{
    public interface IKategoriaService
    {
        Kategoria GetKategoriaById(int id);

        /// <summary>
        /// Wyniki wyszukiwania w kategoriach
        /// </summary>
        IEnumerable<Kategoria> GetCKategoriaList(string searchPhrase);

        /// <summary>
        /// Sugerowane podobne kategorie
        /// </summary>
        IEnumerable<Kategoria> GetFKategoriaPodobnaList(string searchPhrase);
    }

    public class KategoriaService : IKategoriaService
    {
        private AppDbCtx _ctx = new AppDbCtx();

        public Kategoria GetKategoriaById(int id)
        {
            return _ctx.Kategoria.SingleOrDefault(s => s.Id == id);

        }

        public IEnumerable<Kategoria> GetCKategoriaList(string searchPhrase)
        {
            return GetRandomKat();
        }

        private IEnumerable<Kategoria> GetRandomKat()
        {
            var idList = _ctx.Kategoria.Select(id => id.Id).ToList();
            var rand = new Random();
            var lista = new List<Kategoria>();
            for (int i = 0; i < 9; i++)
            {
                var id = rand.Next(1, idList.Count - 1);
                var idItem = idList[id];
                var k = _ctx.Kategoria.SingleOrDefault(s => s.Id == idItem);
                lista.Add(k);
            }
            return lista;
        }

        public IEnumerable<Kategoria> GetFKategoriaPodobnaList(string searchPhrase)
        {
            return GetRandomKat();
        }
    }
}