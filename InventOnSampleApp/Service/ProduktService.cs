﻿using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using InventOnSampleApp.Models;

namespace InventOnSampleApp.Service
{
    public interface IProduktService
    {
        Produkt GetProduktById(int id);

        /// <summary>
        /// Wyniki wyszukiwania
        /// </summary>
        IEnumerable<Produkt> GetDProduktList(string searchPhrase);

        /// <summary>
        /// Wyniki wyszukiwania podobnych produktów
        /// </summary>
        IEnumerable<Produkt> GetEProduktPodobnyList(string searchPhrase);
    }

    public class ProduktService : IProduktService
    {
        private readonly ProduktDao _produktDao;

        public ProduktService(ProduktDao produktDao)
        {
            _produktDao = produktDao;
        }

        public Produkt GetProduktById(int id)
        {
            return _produktDao.GetProduktById(id);
        }

        public IEnumerable<Produkt> GetDProduktList(string searchPhrase)
        {
            var lista = _produktDao.Search(searchPhrase);
            return lista;
        }

        public IEnumerable<Produkt> GetEProduktPodobnyList(string searchPhrase)
        {
            var lista = _produktDao.Search(searchPhrase);
            return lista.Take(6);
        }
    }






}