﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InventOnSampleApp.Models;
using InventOnSampleApp.Service;

namespace InventOnSampleApp.Controllers
{
    public class KategoriaController : Controller
    {
        private readonly KategoriaService _kategoriaService;
        private readonly ProduktService _produktService;
        private readonly WynikiWyszukiwaniaService _wynikiWyszukiwaniaService;

        public KategoriaController(KategoriaService kategoriaService, ProduktService produktService, WynikiWyszukiwaniaService wynikiWyszukiwaniaService)
        {
            _kategoriaService = kategoriaService;
            _produktService = produktService;
            _wynikiWyszukiwaniaService = wynikiWyszukiwaniaService;
        }

        public ActionResult Index(string searchPhrase = "")
        {
            var model = new KategoriaViewModel()
            {
                A = _wynikiWyszukiwaniaService.GetPww(searchPhrase),
                B = _wynikiWyszukiwaniaService.GetNww(searchPhrase),
                CKategoriaList = _kategoriaService.GetCKategoriaList(searchPhrase).ToList(),
                DProduktList = _produktService.GetDProduktList(searchPhrase).ToList(),
                EProduktPodobnyList = _produktService.GetEProduktPodobnyList(searchPhrase).ToList(),
                FKategoriaPodobnaList = _kategoriaService.GetFKategoriaPodobnaList(searchPhrase).ToList()
            };

            return View(model);
        }

        public ActionResult A(string searchPhrase)
        {
            return View();
        }

        public ActionResult B(string searchPhrase)
        {
            return View();
        }

        public ActionResult Wpp()
        {
            return View();
        }

        public ActionResult Produkt(int id)
        {
            var model = _produktService.GetProduktById(id);
            return View(model);
        }

        public ActionResult Kategoria(int id)
        {
            var model = _kategoriaService.GetKategoriaById(id);
            return View(model);
        }
    }
}
