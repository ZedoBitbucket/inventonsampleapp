namespace InventOnSampleApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_model : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Produkts", "Kategoria_Id", "dbo.Kategorias");
            DropIndex("dbo.Produkts", new[] { "Kategoria_Id" });
            AddColumn("dbo.Produkts", "KategoriaId", c => c.Int(nullable: false));
            AddForeignKey("dbo.Produkts", "KategoriaId", "dbo.Kategorias", "Id", cascadeDelete: true);
            CreateIndex("dbo.Produkts", "KategoriaId");
            DropColumn("dbo.Produkts", "Kategoria_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Produkts", "Kategoria_Id", c => c.Int());
            DropIndex("dbo.Produkts", new[] { "KategoriaId" });
            DropForeignKey("dbo.Produkts", "KategoriaId", "dbo.Kategorias");
            DropColumn("dbo.Produkts", "KategoriaId");
            CreateIndex("dbo.Produkts", "Kategoria_Id");
            AddForeignKey("dbo.Produkts", "Kategoria_Id", "dbo.Kategorias", "Id");
        }
    }
}
