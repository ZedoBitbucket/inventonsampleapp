namespace InventOnSampleApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Kategorias",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nazwa = c.String(nullable: false),
                        Count = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Produkts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nazwa = c.String(nullable: false, maxLength: 50),
                        Opis = c.String(maxLength: 200),
                        CenaMinimalna = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Sprzedający = c.String(),
                        MinimalneZamówienie = c.Int(nullable: false),
                        CzasDostawy = c.Int(nullable: false),
                        ImageUri = c.String(),
                        Kategoria_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Kategorias", t => t.Kategoria_Id)
                .Index(t => t.Kategoria_Id);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Produkts", new[] { "Kategoria_Id" });
            DropForeignKey("dbo.Produkts", "Kategoria_Id", "dbo.Kategorias");
            DropTable("dbo.Produkts");
            DropTable("dbo.Kategorias");
        }
    }
}
