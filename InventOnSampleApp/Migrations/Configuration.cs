using System.Collections.Generic;
using System.Collections.ObjectModel;
using InventOnSampleApp.Models;

namespace InventOnSampleApp.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<InventOnSampleApp.Models.AppDbCtx>
    {

        int _p = 1;
        readonly Random _r = new Random();

        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(InventOnSampleApp.Models.AppDbCtx context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            for (int i = 0; i < 30; i++)
            {
                context.Kategoria.Add(InsertKategoria(i));
            }
        }

        private Kategoria InsertKategoria(int i)
        {
            var k = new Kategoria
            {
                Nazwa = String.Format("Kategoria {0}", i),
                Count = _r.Next(10, 50),
                Produkt = InsertProdukts()
            };

            return k;
        }

        private Collection<Produkt> InsertProdukts()
        {
            int c = _r.Next(5, 30);
            var lista = new Collection<Produkt>();
            for (int i = 0; i < c; i++)
            {
                var produkt = new Produkt
                {
                    CenaMinimalna = (decimal)_r.NextDouble() * _r.Next(10, 1000),
                    CzasDostawy = _r.Next(1, 7),
                    ImageUri = "/Content/Clipboard-4.jpg",
                    MinimalneZamówienie = _r.Next(1, 1000),
                    Nazwa = String.Format("Produkt {0}", _p++),
                    Opis = GetOpis(),
                    Sprzedający = GetSprzedający()
                };
                lista.Add(produkt);
            }
            return lista;
        }

        private string GetSprzedający()
        {
            return String.Format("Sprzedający {0}", _r.Next(1, 10));
        }

        private string GetOpis()
        {
            const string baza = "Metoda nastawiona jest na bezpośrednią komunikację pomiędzy członkami zespołu, minimalizując potrzebę tworzenia dokumentacji. Jeśli członkowie zespołu są w różnych lokalizacjach, to planuje się codzienne kontakty za pośrednictwem dostępnych kanałów komunikacji";
            return baza.Substring(0, _r.Next(5, 200));
        }



    }
}
