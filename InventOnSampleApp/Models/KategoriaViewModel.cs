﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace InventOnSampleApp.Models
{
    public class KategoriaViewModel
    {
        public TextValue A { get; set; }
        public TextValue B { get; set; }
        public List<Kategoria> CKategoriaList { get; set; }
        public List<Produkt> DProduktList { get; set; }
        public List<Produkt> EProduktPodobnyList { get; set; }
        public List<Kategoria> FKategoriaPodobnaList { get; set; }
    }

    public class TextValue
    {
        public string Text { get; set; }
        public string Value { get; set; }    
    }


    public class Produkt
    {
        [Key]
        public int Id { get; set; }

        public int KategoriaId { get; set; }
        [ForeignKey("KategoriaId")]
        public Kategoria Kategoria { get; set; }

        [Required]
        [MaxLength(50)]
        public string Nazwa { get; set; }
        [MaxLength(200)]
        public string Opis { get; set; }
        public decimal CenaMinimalna { get; set; }
        public string Sprzedający { get; set; }
        public int MinimalneZamówienie { get; set; }
        public int CzasDostawy { get; set; }
        public string ImageUri { get; set; }
    }

    public class Kategoria
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Nazwa { get; set; }
        public int Count { get; set; }

        public virtual ICollection<Produkt> Produkt { get; set; }
    }


    public class AppDbCtx : DbContext
    {
        public DbSet<Kategoria> Kategoria { get; set; }
        public DbSet<Produkt> Produkt { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingEntitySetNameConvention>();
        }
    }


}